//
//  Rate.h
//  Dijkstra Converter
//
//  Created by Alex Zagrebin on 10/05/2019.
//  Copyright © 2019 Alexey Zagrebin. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Rate : NSObject

@property (nonatomic, copy) NSString *from;
@property (nonatomic, copy) NSString *to;
@property (nonatomic, copy) NSString *rate;

- (instancetype) initWithRate: (NSDictionary*) rate;
- (BOOL) isValid;

@end

NS_ASSUME_NONNULL_END
