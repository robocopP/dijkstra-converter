//
//  AppDelegate.h
//  Dijkstra Converter
//
//  Created by Alex Zagrebin on 09/05/2019.
//  Copyright © 2019 Alexey Zagrebin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

