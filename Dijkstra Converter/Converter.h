//
//  Converter.h
//  Dijkstra Converter
//
//  Created by Alex Zagrebin on 09/05/2019.
//  Copyright © 2019 Alexey Zagrebin. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Converter : NSObject

- (instancetype) initWithRatesPlist: (NSString*) sName;
- (NSString*) toGBP: (id) value withQuote: (NSString*) quote;
- (NSString*) toGBPSumOfTransactions: (NSArray*) transactions;

@end

NS_ASSUME_NONNULL_END
