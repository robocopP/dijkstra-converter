//
//  Converter.m
//  Dijkstra Converter
//
//  Created by Alex Zagrebin on 09/05/2019.
//  Copyright © 2019 Alexey Zagrebin. All rights reserved.
//

#import "Converter.h"
#import "MJDijkstra/MJDijkstra.h"
#import "Helper.h"
#import "Transaction.h"
#import "Rate.h"

@interface Converter ()

@property (nonatomic, strong) NSMutableDictionary *ratesPaths;
@property (nonatomic, strong) NSMutableDictionary *ratesAmount;

@end

@implementation Converter

-(instancetype)initWithRatesPlist:(NSString *)sName {
    self = [super init];
    if (self) {
        NSString *plistPath2 = [[NSBundle mainBundle] pathForResource:sName ofType:@"plist"];
        NSMutableArray *rates = [[NSMutableArray alloc] initWithContentsOfFile: plistPath2];
        
        self.ratesPaths = @{}.mutableCopy;
        self.ratesAmount = @{}.mutableCopy;
        for (NSDictionary *rate in rates) {
            Rate *oRate = [[Rate alloc] initWithRate:rate];
            
            if (oRate.isValid) {
                NSString *from = oRate.from.uppercaseString;
                NSString *to = oRate.to.uppercaseString;
                
                NSMutableDictionary *tempDict;
                if ((tempDict = [self.ratesPaths objectForKey:from])) {
                    [tempDict setObject:@1 forKey:to];
                    
                } else {
                    tempDict = @{to:@1}.mutableCopy;
                    [self.ratesPaths setObject:tempDict forKey:from];
                }
                
                [self.ratesAmount setObject:oRate.rate forKey:[NSString stringWithFormat:@"%@/%@", from, to]];
            }
            
        }
    }
    return self;
}

-(NSString *)toGBP:(id)value withQuote: (NSString*) quote withFormat: (int) format {
    NSString *start = quote;
    NSString *end = @"GBP";
    
    NSArray *path;
    @try {
         path = shortestPath(self.ratesPaths, start, end);
        
    } @catch (NSException *exception) {
        NSLog(@"unexpectable error");
    }
    
    NSMutableArray *rawRates = @[].mutableCopy;
    double amount = [value doubleValue];
    
    for (NSInteger i = 0; i < path.count; i++) {
        if (i+1 == path.count) {
            break;
        }
        
        NSString *lastQuote = path[i];
        NSString *nextQuote = path[i+1];
        
        [rawRates addObject:[self.ratesAmount objectForKey:[NSString stringWithFormat:@"%@/%@", lastQuote, nextQuote]]];
    }
    
    for (NSString *rawRate in rawRates) {
        double dRawRate = [rawRate doubleValue];
        amount *= dRawRate;
    }
    
    return [NSString stringWithFormat:@"%.*f", format, amount];
}

- (NSString *)toGBP:(id)value withQuote: (NSString*) quote {
    return [self toGBP:value withQuote:quote withFormat:2];
}

-(NSString *)toGBPSumOfTransactions:(NSArray *)transactions {
    double sum = 0.0;
    for (Transaction *transaction in transactions) {
        NSString *transactionCurrency = transaction.currency;
        NSString *transactionAmount = transaction.amount;
        
        sum += [self toGBP:transactionAmount withQuote:transactionCurrency withFormat:15].doubleValue;
    }
    
    return [NSString stringWithFormat:@"%.02f", sum];
}

@end
