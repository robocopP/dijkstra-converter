//
//  Transaction.h
//  Dijkstra Converter
//
//  Created by Alex Zagrebin on 10/05/2019.
//  Copyright © 2019 Alexey Zagrebin. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Transaction : NSObject

@property (nonatomic, copy) NSString *amount;
@property (nonatomic, copy) NSString *currency;
@property (nonatomic, copy) NSString *sku;

- (instancetype) initWithTransaction: (NSDictionary*) rate;
- (BOOL) isValid;

@end

NS_ASSUME_NONNULL_END
