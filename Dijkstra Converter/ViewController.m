//
//  ViewController.m
//  Dijkstra Converter
//
//  Created by Alex Zagrebin on 09/05/2019.
//  Copyright © 2019 Alexey Zagrebin. All rights reserved.
//

#import "ViewController.h"
#import "Converter.h"
#import "Helper.h"
#import "Transaction.h"

static NSString* transactionsPlistName = @"transactions";
static NSString* ratesPlistName = @"rates";

@interface ViewController ()

@property (nonatomic, strong) Converter *converter;
@property (nonatomic, strong) NSArray *transactions;
@property (nonatomic, strong) NSString *skuKey;

@property (nonatomic, strong) NSMutableDictionary *skuTransactions;
@property (nonatomic, strong) NSArray *sortedSkuKeys;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSString *plistPath = [[NSBundle mainBundle] pathForResource:transactionsPlistName ofType:@"plist"];
        NSMutableArray *transactions = [[NSMutableArray alloc] initWithContentsOfFile: plistPath];
        
        self.skuTransactions = @{}.mutableCopy;
        for (NSDictionary *transaction in transactions) {
            Transaction *oTransaction = [[Transaction alloc] initWithTransaction:transaction];
            
            NSMutableArray *tempArr;
            if (oTransaction.isValid && (tempArr = [self.skuTransactions objectForKey:oTransaction.sku])) {
                [tempArr addObject:oTransaction];
                
            } else {
                tempArr = @[oTransaction].mutableCopy;
                [self.skuTransactions setObject:tempArr forKey:oTransaction.sku];
            }
        }
        
        self.sortedSkuKeys = [[self.skuTransactions allKeys] sortedArrayUsingSelector: @selector(compare:)];
        
        [self.tableView reloadData];
    });
    
    if ([self isOnDetail]) {
        self.title = [NSString stringWithFormat:@"Transactions for %@", self.skuKey.uppercaseString];
    }
}

- (BOOL) isOnMenu {
    return !(self.converter);
}

- (BOOL) isOnDetail {
    return (self.converter);
}

- (NSArray*) sortedTransactionsForIndexPath: (NSIndexPath*) indexPath {
    return [self.skuTransactions objectForKey:[self.sortedSkuKeys objectAtIndex:indexPath.row]];
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    formatter.numberStyle = NSNumberFormatterCurrencyStyle;
    NSString *formatterString = [formatter stringFromNumber:@([self.converter toGBPSumOfTransactions:self.transactions].doubleValue)];
    NSString *escapedString = [formatterString stringByReplacingOccurrencesOfString:@"$" withString:@""];
    
    return ([self isOnDetail]) ? [NSString stringWithFormat:@"Total: £%@", escapedString] : @"";
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return ([self isOnMenu]) ? self.skuTransactions.allKeys.count : self.transactions.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *cellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    if (self.isOnMenu) {
        NSString *skuTransactionStr = [self.sortedSkuKeys objectAtIndex:indexPath.row];
        NSInteger countSkuTransacctions = [[self.skuTransactions objectForKey:skuTransactionStr] count];
    
        cell.textLabel.text = skuTransactionStr;
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ transactions", @(countSkuTransacctions).stringValue];
        
    } else {
        NSString *transactionCurrency = [[self.transactions objectAtIndex:indexPath.row] currency];
        NSString *transactionAmount = [[self.transactions objectAtIndex:indexPath.row] amount];
        
        cell.textLabel.text = [NSString stringWithFormat:@"%.2f %@", transactionAmount.doubleValue, transactionCurrency];
        cell.detailTextLabel.text = [NSString stringWithFormat:@"£%@", [self.converter toGBP:transactionAmount withQuote:transactionCurrency]];
        
    }
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44.f;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.isOnMenu) {
        Converter *converter = [[Converter alloc] initWithRatesPlist:ratesPlistName];
        UIStoryboard *st = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        ViewController *controller = [st instantiateViewControllerWithIdentifier:@"DetailedController"];
        controller.converter = converter;
        controller.transactions = [self sortedTransactionsForIndexPath:indexPath];
        controller.skuKey = [self.sortedSkuKeys objectAtIndex:indexPath.row];
        
        [self.navigationController pushViewController:controller animated:YES];
    }
}

@end
