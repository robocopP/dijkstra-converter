//
//  Rate.m
//  Dijkstra Converter
//
//  Created by Alex Zagrebin on 10/05/2019.
//  Copyright © 2019 Alexey Zagrebin. All rights reserved.
//

#import "Rate.h"
#import "Helper.h"

@implementation Rate

- (instancetype)initWithRate:(NSDictionary *)rate {
    self = [super init];
    if (self) {
        self.from = rate[@"from"];
        self.rate = rate[@"rate"];
        self.to = rate[@"to"];
    }
    return self;
}

-(BOOL)isValid {
    return (self.from.isExistAndItIsNonNullLengthString && self.rate.isExistAndItIsNonNullLengthString && self.to.isExistAndItIsNonNullLengthString) ? (self.rate.doubleValue > 0) : NO;
}

@end
