//
//  Helper.m
//  Dijkstra Converter
//
//  Created by Alex Zagrebin on 09/05/2019.
//  Copyright © 2019 Alexey Zagrebin. All rights reserved.
//

#import "Helper.h"

@implementation NSObject (Nullable)

- (BOOL) isExist {
    return (self != nil || self != Nil || self != NULL || self != [NSNull null]);
}

- (BOOL) isExistAndItIsString {
    return (self.isExist && [self isKindOfClass:[NSString class]]);
}

- (BOOL) isExistAndItIsNonNullLengthString {
    return (self.isExistAndItIsString && ((NSString*) self).length > 0);
}

@end
