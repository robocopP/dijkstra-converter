//
//  Transaction.m
//  Dijkstra Converter
//
//  Created by Alex Zagrebin on 10/05/2019.
//  Copyright © 2019 Alexey Zagrebin. All rights reserved.
//

#import "Transaction.h"
#import "Helper.h"

@implementation Transaction

- (instancetype)initWithTransaction:(NSDictionary *)rate {
    self = [super init];
    if (self) {
        self.amount = rate[@"amount"];
        self.currency = rate[@"currency"];
        self.sku = rate[@"sku"];
    }
    return self;
}

-(BOOL)isValid {
    return (self.amount.isExistAndItIsNonNullLengthString && self.currency.isExistAndItIsNonNullLengthString && self.sku.isExistAndItIsNonNullLengthString) ? (self.amount.doubleValue > 0) : NO;
}

@end
