//
//  Helper.h
//  Dijkstra Converter
//
//  Created by Alex Zagrebin on 09/05/2019.
//  Copyright © 2019 Alexey Zagrebin. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSObject (Nullable)

- (BOOL) isExist;
- (BOOL) isExistAndItIsString;
- (BOOL) isExistAndItIsNonNullLengthString;

@end

NS_ASSUME_NONNULL_END
